<?php namespace Threef\Entree\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Events\Dispatcher;

class Threef
{
    /**
     * The authenticator implementation.
     *
     * @var \Illuminate\Contracts\Auth\Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     */
    public function __construct(Guard $auth, Dispatcher $dispatcher)
    {
        $this->auth = $auth;
        $this->dispatcher = $dispatcher;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $this->beforeHandle();

        if (!$this->auth->check()) {
            return redirect(handles('app::login'));
        }

        // $this->afterHandle();

        return $next($request);
    }

    /**
     * Before handle.
     *
     * @return void
     */
    protected function beforeHandle()
    {
        $this->dispatcher->fire('threef.ready');
    }

    /**
     * After handle.
     *
     * @return void
     */
    protected function afterHandle()
    {
        $this->dispatcher->fire('threef.done');
    }

}
