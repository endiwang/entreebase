<?php namespace Threef\Entree\Http\User;

use Orchestra\Model\Role as ModelRole;

/**
 * Role Model extends Orchestra\Model\Role
 *
 * @package threef/entree
 * @author
 **/
class Role extends ModelRole
{
    // Administrator
    // Member
    // Admin
    // Pegawai CAROS
    // PPR HQ
    // PPR Negeri
    // IO HQ
    // IO Negeri
    // Ketua CAROS
    // Pengarah BPRTM
    // Pengarah Negeri
    // Ketua Siasatan Negeri
    // Timbalan Pengarah Operasi
    // Pegawai SPRM HQ
    // Pegawai SPRM Negeri
    // Pengarah Bahagian
    // Ketua Cawangan
    // PPR Cawangan Negeri
    // Ketua Cawangan Negeri
    // Timbalan Pengarah BPRTM
    // Timbalan Pengarah
    // PPR Cawangan HQ


    /**
     * Get  roles Administrator
     *
     * @return $this|null
     */
    public static function administrator()
    {
        return static::find(1);
    }

    /**
     * Get  roles Member
     *
     * @return $this|null
     */
    public static function member()
    {
        return static::find(2);
    }
    /**
     * Get  roles
     *
     * @return $this|null
     */
    public static function admin()
    {
        return static::find(3);
    }
    /**
     * Get  roles Pegawai CAROS
     *
     * @return $this|null
     */
    public static function CarosOfficer()
    {
        return static::find(4);
    }
    /**
     * Get  roles PPR HQ
     *
     * @return $this|null
     */
    public static function pprHQ()
    {
        return static::find(5);
    }
    /**
     * Get  roles PPR Negeri
     *
     * @return $this|null
     */
    public static function pprNegeri()
    {
        return static::find(6);
    }
    /**
     * Get  roles IO HQ
     *
     * @return $this|null
     */
    public static function ioHQ()
    {
        return static::find(7);
    }
    /**
     * Get SPRM roles IO Negeri
     *
     * @return $this|null
     */
    public static function ioNegeri()
    {
        return static::find(8);
    }
    /**
     * Get SPRM Ketua CAROS
     *
     * @return $this|null
     */
    public static function ketuaCAROS()
    {
        return static::find(9);
    }
    /**
     * Get SPRM roles Pengarah BPRTM
     *
     * @return $this|null
     */
    public static function pengarahBPRTM()
    {
        return static::find(10);
    }
    /**
     * Get SPRM roles Pengarah Negeri
     *
     * @return $this|null
     */
    public static function pengarahNegeri()
    {
        return static::find(11);
    }

    /**
     * Get SPRM roles Ketua Siasatan Negeri
     *
     * @return $this|null
     */
    public static function ketuaSiasatanNegeri()
    {
        return static::find(12);
    }

    /**
     * Get SPRM roles Timbalan Pengarah Operasi
     *
     * @return $this|null
     */
    public static function TimbPengOperasi()
    {
        return static::find(13);
    }

    /**
     * Get SPRM roles Pegawai SPRM HQ
     *
     * @return $this|null
     */
    public static function PegSprmHQ()
    {
        return static::find(14);
    }

    /**
     * Get SPRM roles Pegawai SPRM Negeri
     *
     * @return $this|null
     */
    public static function PegawaiSPRMNegeri()
    {
        return static::find(15);
    }

    /**
     * Get SPRM roles Pengarah Bahagian
     *
     * @return $this|null
     */
    public static function pengBahagian()
    {
        return static::find(16);
    }

    /**
     * Get SPRM roles Ketua Cawangan
     *
     * @return $this|null
     */
    public static function ketuaCaw()
    {
        return static::find(17);
    }

    /**
     * Get SPRM roles PPR Cawangan Negeri
     *
     * @return $this|null
     */
    public static function pprCawNeg()
    {
        return static::find(18);
    }

    /**
     * Get SPRM roles Ketua Cawangan Negeri
     *
     * @return $this|null
     */
    public static function ketuaCawNeg()
    {
        return static::find(19);
    }

    /**
     * Get SPRM roles Timbalan Pengarah BPRTM
     *
     * @return $this|null
     */
    public static function timbPengBPRTM()
    {
        return static::find(20);
    }

    /**
     * Get SPRM roles Timbalan Pengarah
     *
     * @return $this|null
     */
    public static function timbPeng()
    {
        return static::find(21);
    }

    /**
     * Get SPRM roles PPR Cawangan HQ
     *
     * @return $this|null
     */
    public static function pprCawHQ()
    {
        return static::find(22);
    }

    /**
     * Get SPRM roles PPR  HQ Siasatan
     *
     * @return $this|null
     */
    public static function pprHQSiasatan()
    {
        return static::find(23);
    }



} // END class Role extends ModelRole
