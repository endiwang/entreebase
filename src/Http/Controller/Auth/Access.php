<?php namespace Threef\Entree\Http\Controller\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Orchestra\Foundation\Processor\AuthenticateUser;
use Orchestra\Foundation\Processor\DeauthenticateUser;
use Orchestra\Contracts\Auth\Command\ThrottlesLogins as ThrottlesCommand;
use Orchestra\Contracts\Auth\Listener\AuthenticateUser as AuthenticateListener;
use Orchestra\Contracts\Auth\Listener\ThrottlesLogins as ThrottlesListener;
use Orchestra\Contracts\Auth\Listener\DeauthenticateUser as DeauthenticateListener;

class Access extends Controller implements AuthenticateListener,DeauthenticateListener, ThrottlesListener
{

    /**
     * POST Login the user.
     *
     * POST (:orchestra)/login
     *
     * @return mixed
     */
    public function login(Request $request, AuthenticateUser $authenticate, ThrottlesCommand $throttles )
    {
        $input = $request->only(['email', 'password', 'remember']);
        $throttles->setRequest($request)->setLoginKey('email');

        return $authenticate->login($this, $input, $throttles);
    }

    /**
     * POST Login the user using username.
     *
     * POST (:orchestra)/login
     *
     * @return mixed
     */
    public function loginfront(Request $request, AuthenticateUser $authenticate, ThrottlesCommand $throttles )
    {


        $input = $request->only(['username', 'password', 'remember']);
        $throttles->setRequest($request)->setLoginKey('username');


        return $authenticate->loginfront($this, $input, $throttles);
    }

    /**
     * Logout the user.
     *
     * DELETE (:bundle)/login
     *
     * @return mixed
     */
    public function logout(DeauthenticateUser $deauthenticate)
    {
        return $deauthenticate->logout($this);
    }

    /**
     * GET default entree landing page
     *
     * @return mixed
     **/
    public function home()
    {
        return view('entree::entree.home');
    }

    /**
     * Response to user log-in trigger failed validation .
     *
     * @param  \Illuminate\Support\MessageBag|array  $errors
     *
     * @return mixed
     */
    public function userLoginHasFailedValidation($errors){

        return $this->redirectWithErrors(handles('entree::login'), $errors);
    }

    /**
     * Response to user log-in trigger has failed authentication.
     *
     * @param  array  $input
     *
     * @return mixed
     */
    public function userLoginHasFailedAuthentication(array $input){

        $message = trans('orchestra/foundation::response.credential.invalid-combination');

        return $this->redirectWithMessage(handles('entree::login'), $message, 'error')->withInput();
    }

    /**
     * Redirect the user after determining they are locked out.
     *
     * @param  array  $input
     * @param  int  $seconds
     *
     * @return mixed
     */
    public function sendLockoutResponse(array $input, $seconds){

        $message = trans('auth.throttle', ['seconds' => $seconds]);

        return $this->redirectWithMessage(handles('entree::login'), $message, 'error')->withInput();
    }

    /**
     * Response to user has logged in successfully.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     *
     * @return mixed
     */
    public function userHasLoggedIn(Authenticatable $user){

        messages('success', trans('orchestra/foundation::response.credential.logged-in'));

        event('threef.user.login', [$user]);

        return Redirect::intended(handles('entree::home'));
    }

    /**
     * Response to user has logged out successfully.
     *
     * @return mixed
     */
    public function userHasLoggedOut(){

        messages('success', trans('orchestra/foundation::response.credential.logged-out'));

        return Redirect::intended(handles(Input::get('redirect', 'entree::login')));
    }

}
