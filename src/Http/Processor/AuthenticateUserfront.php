<?php namespace Threef\Entree\Http\Processor;

use Illuminate\Support\Arr;
use Orchestra\Contracts\Auth\Guard;
use Orchestra\Model\User as Eloquent;
use Orchestra\Contracts\Auth\Command\AuthenticateUser as Command;
use Orchestra\Foundation\Validation\AuthenticateUser as Validator;
use Orchestra\Contracts\Auth\Listener\AuthenticateUser as Listener;
use Orchestra\Contracts\Auth\Command\ThrottlesLogins as ThrottlesCommand;
use Orchestra\Foundation\Processor\AuthenticateUser as OrchestraAuthenticate;

// class AuthenticateUserfront extends Authenticate implements Command
class AuthenticateUserfront extends OrchestraAuthenticate implements Command
{

    public function loginfront(Listener $listener, array $input, ThrottlesCommand $throttles = null)
    {

        $validation = $this->validator->on('loginfront')->with($input);

        // Validate user login, if any errors is found redirect it back to
        // login page with the errors.
        if ($validation->fails()) {

            return $listener->userLoginHasFailedValidation($validation->getMessageBag());
        }

        if ($this->hasTooManyAttempts($throttles)) {
            return $this->handleUserHasTooManyAttempts($listener, $input, $throttles);
        }

        // check for status
        // if (! $this->checkStatus($input)){

        // }

        if ($this->authenticate($input)) {
            return $this->handleUserWasAuthenticated($listener, $input, $throttles);
        }

        return $this->handleUserFailedAuthentication($listener, $input, $throttles);
    }
}
